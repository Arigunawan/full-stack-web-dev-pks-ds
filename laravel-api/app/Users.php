<?php

namespace App;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = ['id'];
    protected $fillable = ['username'];
    protected $fillable = ['email'];
    protected $fillable = ['name'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if ( empty($model->id)){
                $model->id = Str::uuid();
            }
        });
    }
}