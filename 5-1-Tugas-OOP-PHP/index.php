<?php
// Parent class
abstract class Hewan
{
    public $name;
    public $darah;
    public $jumlahkaki;
    public $keahlian;

    public function __construct($name)
    {
        $this->name = $name;
    }
    abstract public function atraksi();
}
abstract class Fight
{
    public $name;
    public $attackPower;
    public $defencePower;


    public function __construct($name)
    {
        $this->name = $name;
    }
    abstract public function serang();
    abstract public function diserang();
}

// Child classes Hewan
class Elang extends Hewan
{
    public function atraksi(): string
    {
        return $this->name .  " sedang lari cepat";
    }
}

class Harimau extends Hewan
{
    public function atraksi(): string
    {
        return  $this->name . " sedang terbang tinggi";
    }
}

// Child classes Fight
class Elang1 extends Fight
{
    public function serang(): string
    {
        return $this->name .  " sedang menyerang harimau 1";
    }
    public function diserang(): string
    {
        return $this->name .  " sedang diserang ";
    }
}

class Harimau1 extends Fight
{
    public function serang(): string
    {
        return $this->name .  " sedang lari cepat";
    }
    public function diserang(): string
    {
        return $this->name .  " sedang lari cepat";
    }
}

// Create objects from the child classes Hewan
$elang = new elang("Elang");
echo $elang->atraksi();
echo "<br>";

$harimau = new harimau("Harimau");
echo $harimau->atraksi();
echo "<br>";

// Create objects from the child classes Fight
$elang1 = new elang1("Elang1");
echo $elang1->serang();
echo "<br>";

$harimau1 = new harimau1("Harimau1");
echo $harimau1->serang();
echo "<br>";

// Create objects from the child classes Fight
$elang1 = new elang1("Elang1");
echo $elang1->diserang();
echo "<br>";

$harimau1 = new harimau1("Harimau1");
echo $harimau1->diserang();
echo "<br>";